# CodeLounge GitLab CI Templates

The CI templates provide pre-defined jobs for different tasks. The templates can be [included](https://docs.gitlab.com/ee/ci/yaml/includes.html) in the following way in a GitLab project:
```yml
include:
  - project: si-codelounge/public/gitlab-ci-templates
    file: /codelounge.gitlab-ci.yml
```
This will include the general CodeLounge template which includes Auto-Deploy and SonarQube.

In addition the `stage` of the jobs need to be defined in the project's gitlab-ci file (see which for each template below). 

A template might also require certain [CI variables](https://docs.gitlab.com/ee/ci/variables/) to be setup. See the specific template documentation for details. They can be configured per project or group.

The templates are prepared for working with the [`workflow`](https://docs.gitlab.com/ee/ci/yaml/README.html#workflow) GitLab CI keyword. To make this work we only use [`rules`](https://docs.gitlab.com/ee/ci/yaml/README.html#rules) to determine if a job should be triggered or not. This is a replacement for `only/except` keywords (they are not compatible). This allows us to use the [`workflow:rules`](https://docs.gitlab.com/ee/ci/yaml/README.html#workflowrules-templates) templates defined by GitLab.

### Deploying at USI

If you need to deploy your service in a USI machine whose port 22 is not reachable from outside USI, you can directly use a dedicated job that will pick up a runner on `dev.si.usi.ch`.

In this case, include the dedicated template:

```yml
include:
  - project: si-codelounge/public/gitlab-ci-templates
    file: /codelounge-deploy-at-usi.gitlab-ci.yml
```

In this case, all deployment jobs will be run on `dev` runners. For details on how this works and how it can be customized, see the [deployment section](#deploy).

## Developing New Templates

The easiest way to develop (or modify existing templates) is to create a MR on this repository, then include the new (or modified) template in a specific project where you want to test it. This can be done by specifying the branch in this project that was created when the MR was created. Here is an example:
```yml
include:
  - project: si-codelounge/public/gitlab-ci-templates
    ref: 6-add-sq-template
    file: /stages/sonarqube.gitlab-ci.yml
```

## Templates

### CodeLounge Template

- stages: `sonarqube`, `release`, `deploy`
- include:
```yml
- project: si-codelounge/public/gitlab-ci-templates
    file: /codelounge.gitlab-ci.yml
```

The CodeLounge template includes the [Pipelines for Merge-Requests template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Workflows/MergeRequest-Pipelines.gitlab-ci.yml) from GitLab, and also the SonarQube and Auto-Deploy templates (see below for details). It provides the basic setup for CI for any CodeLounge project.

### Override Specific Template Jobs

The template functionality works by merging the template with the `gitlab-ci.yml` of the project that uses the template. This means it is possible to override the different jobs. See the [GitLab documentation](https://docs.gitlab.com/ee/ci/yaml/includes.html#override-included-configuration-values) for how to do it.

Note: when you override a key inside a job the complete value of the key will be replaced, e.g.,

```yaml
# template file with job
my-job:
  stage: build
  script:
    - run first
    - run second

# project using template
my-job:
  script:
    - run third

# becomes (through merging)
my-job:
  stage: build
  script:
    - run third
```

Using the [pipeline editor](https://docs.gitlab.com/ee/ci/pipeline_editor/) in a project it is possible to [view the merged version](https://docs.gitlab.com/ee/ci/pipeline_editor/#view-full-configuration) of the `gitlab-ci.yaml` file.

See the [GitLab documentation](https://docs.gitlab.com/ee/ci/yaml/includes.html#override-included-configuration-values) for more examples.


### SonarQube Template

- stage: `sonarqube`
- include:
```yml
- project: si-codelounge/public/gitlab-ci-templates
    file: /stages/sonarqube.gitlab-ci.yml
```

SonarQube can be integrated with GitLab as described in the [documentation](https://docs.sonarqube.org/latest/analysis/gitlab-integration/) (see section `Importing your GitLab projects into SonarQube`). This works in general how described, however, there are a couple of differences in the setup that are needed in order for it to work well with our setup. The necessary environment variables have been setup in the CodeLounge group (`SONAR_TOKEN` and `SONAR_HOST_URL`).

To make MR decorations work you need to include the [MergeRequest-Pipelines template](https://docs.gitlab.com/ee/ci/yaml/README.html#workflowrules-templates) in your CI yaml, like this:
```yml
include:
  - template: 'Workflows/MergeRequest-Pipelines.gitlab-ci.yml'
```

To add the project we do it from the CI pipeline instead of through the SonarQube web interface. This is so that we have better control of the name of the project. Here are the steps:
1. A SonarQube configuration file needs to be added in the root of the project with the name `sonar-project.properties`. For example for a TypeScript project:
    ```
    # --- required for ci template ---

    # must be unique in a given SonarQube instance
    sonar.projectKey=gitlab-dev-si-${env.CI_PROJECT_ID}
    sonar.qualitygate.wait=true

    # defaults to project key
    sonar.projectName=${env.CI_PROJECT_PATH}

    # defaults to 'not provided'
    # sonar.projectVersion=${env.CI_COMMIT_TAG}

    # --- additional properties ---
    
    # Path is relative to the sonar-project.properties file. Defaults to .
    sonar.sources=src
    sonar.tests=src
    sonar.exclusions=**/*.spec.ts,**/*.test.ts,**/graphql/models.tsx
    sonar.test.inclusions=**/*.spec.ts,**/*.test.ts

    # special config until TS 4.1 is supported
    # tentative to be included in 8.7 (Feb 2021)
    # https://github.com/SonarSource/SonarJS/issues/2383
    sonar.typescript.tsconfigPath=./tsconfig.sonar.json

    # test coverage
    sonar.javascript.lcov.reportPaths=coverage/lcov.info
    
    # Encoding of the source code. Default is default system encoding
    sonar.sourceEncoding=UTF-8
    ```
2. Run the pipeline. This will add the project to SonarQube.
3. Add the MR decoration in order to see the results directly in the MR (section `Adding merge request decoration to a manually created or existing project` in the SonarQube docs). The GitLab Project ID can be found on GitLab or also on SonarQube as the numeric part of the Project Key: (select the project) `Project Information > Project Key`.
> To add merge request decoration to a manually created or existing project, make sure your global ALM Integration settings are set as shown in the Importing your GitLab projects into SonarQube section above, and set the following project settings at Project Settings > General Settings > Pull Request Decoration:
>
> Configuration name – The configuration name that corresponds to your GitLab instance.
> Project ID – your GitLab Project ID found in GitLab
4. Re-running the pipeline should now show the decoration in the MR (see note above about including the MR pipelines template).

### Deploy Template
- stages: `release` and `deploy`
- include:
```yml
- project: si-codelounge/public/gitlab-ci-templates
    file: /stages/deploy.gitlab-ci.yml
```

The template also provides extension points for running jobs as `review`, `staging` or `production_tag`. These can be used like so:
```yml
some_job:
  extends: .review
```
This allows a specific job to be run only when releasing and deploying to `review`. This is helpful in case the build step needs to be different depending on where it will be deployed.

- `review` - used to deploy to review URL on staging machine. Unique deployment URL for each MR.
- `staging` - used to deploy to staging URL on staging machine. Same URL for every deployment.
- `production_tag` - is triggered whenever a branch is tagged with a version that follows the following regex: `^v[0-9]+[.][0-9]+[.][0-9]+(-rc[0-9]+)?$` (e.g., v1.1.0, v1.1.0-rc1, v10.112.2). Intended to be used only on the `main` branch. Deployed to production URL on production machine (can be the same as staging machine, see variables below).

### Deploy

Prefer `CI_COMMIT_REF_SLUG` over `CI_COMMIT_REF_NAME` since it is optimized for [URLs, host names and domain names](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html).

By default, the deployment jobs run on `gitlab.com` shared runners, but this can be customized using the `DEPLOY_JOB_TAG` variable.

#### Required CI variables

- `PROJECT_NAME`: the name of the CodeLounge project (needed since we can't always get it from the predefined CI variables), e.g., `stringsart`

For review and staging:
- `STAGING_HOST`: the host where to the deploy, e.g., `stringsart.inf.usi.ch`
- `STAGING_USER`: the user on the host (for ssh access), e.g., `gitlab`
- `STAGING_SSH_PRIVATE_KEY`: the private key of the user (for ssh access)
- `STAGING_HOST_PATH`: the path on the host, e.g., `/opt/codelounge/stringsart.inf.usi.ch`
- For Docker-Compose
  - `STAGING_NETWORK_NAME`: the name of the docker network, e.g., `stringsartinfusich_stringsart`

For production:
- `PRODUCTION_HOST`: the host where to the deploy, e.g., `stringsart.inf.usi.ch`
- `PRODUCTION_USER`: the user on the host (for ssh access), e.g., `gitlab`
- `PRODUCTION_SSH_PRIVATE_KEY`: the private key of the user (for ssh access)
- `PRODUCTION_HOST_PATH`: the path on the host, e.g., `/opt/codelounge/stringsart.inf.usi.ch`
- For Docker-Compose
  - `PRODUCTION_NETWORK_NAME`: the name of the docker network, e.g., `stringsartinfusich_stringsart`

#### Optional CI variables.

- `DEPLOY_JOB_TAG`: This defines a custom job tag for the deployment jobs, so that a given runner or runner pool will be picked up for these jobs. You can define it at any level: for a specific job (by extending the specific job), or for all deployment jobs. If you need a deployment at USI, you do not need to override this variable, but you should use the [dedicated template](#deploying-at-usi).

#### Docker

The scripts will build a docker container where the name is based on the `CI_REGISTRY_IMAGE` variable (see [gitlab docs](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)).

The following variables are used to determine the full name (incl. tag) of the docker image:
- `REVIEW_DOCKER_IMAGE_NAME`: `${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}` (e.g., `registry.gitlab.com/si-codelounge/research-and-development/stringarts/frontend:10-a-new-feature`)
- `STAGING_DOCKER_IMAGE_NAME_LATEST`: `${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}` (e.g., `registry.gitlab.com/si-codelounge/research-and-development/stringarts/frontend:dev`)
- `PRODUCTION_DOCKER_IMAGE_NAME_LATEST`: `${CI_REGISTRY_IMAGE}:latest` (e.g., `registry.gitlab.com/si-codelounge/research-and-development/stringarts/frontend:latest`)
- `PRODUCTION_DOCKER_IMAGE_NAME_TAG`: `${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}` (e.g., `registry.gitlab.com/si-codelounge/research-and-development/stringarts/frontend:v1.1.0`)

To deploy the docker image it is necessary to create 3 docker-compose files (one for each stage in a folder called `deployment` in the root of the project):
- `deployment/docker-compose.review.yml`
- `deployment/docker-compose.staging.yml`
- `deployment/docker-compose.production.yml`

These files should contain the complete docker configuration that is necessary for the deployment (e.g., environment variables, ports, database). For example:
```yml
version: "3"

services:
  service:
    image: ${PRODUCTION_DOCKER_IMAGE_NAME_LATEST}
    container_name: ${CONTAINER_NAME}
    restart: always

networks:
  default:
      name: ${PRODUCTION_NETWORK_NAME}
      external: true
```

The `CONTAINER_NAME` variable is automatically generated by the deploy script to help with identifying the container.

**Note on storing data on docker host**

For staging and production bind mounts or voumes can be used. Using bind mounts might lead to permission problems if docker creates the folders as they are being created as `root`. To avoid this please manually create the necessary folders on the host machine before deploying.

For review deployments it is recommended to use volumes instead of bind mounts. When using volumes there are no permission issues (e.g., they don't need to be created upfront). Volumes will also be automatically cleaned up when the deployment is stopped or re-deployed.

#### Configurable variables 
in gitlab-ci.yml of the project ([VARIABLE_NAME]: [DEFAULT_VALUE]):
- `REVIEW_REVERSE_PROXY_PATH: /review/${CI_PROJECT_NAME}/${CI_COMMIT_REF_SLUG}`
- `STAGING_REVERSE_PROXY_PATH: /staging/${CI_PROJECT_NAME}`
- `PRODUCTION_REVERSE_PROXY_PATH: /${CI_PROJECT_NAME}` (no trailing slash because it is added automatically)
- `REVIEW_GITLAB_ENV_URL: https://${STAGING_HOST}/review/${CI_PROJECT_NAME}/${CI_COMMIT_REF_SLUG}/`
- `STAGING_GITLAB_ENV_URL: https://${STAGING_HOST}/staging/${CI_PROJECT_NAME}/`
- `PRODUCTION_GITLAB_ENV_URL: https://${PRODUCTION_HOST}/${CI_PROJECT_NAME}/`

Alternatively, some non-environment specific variables that may be needed to be redefined:
-  `REVERSE_PROXY_CONTAINER_NAME: reverse_proxy`
-  `REVERSE_PROXY_CONFIG_SUBPATH: auto-deployment`


#### Reverse Proxy Configuration

The deploy will create unique configurations for each deploy. For `review` there is one per branch or MR (per project). For `staging` and `production` there is one per project. The files have the following names:

- `review.[project name].[branch].conf`
- `staging.[project name].conf`
- `production.[project name].conf`

These files should be used with `include` in the main nginx config file. Note that the order of the `include` statements are important since nginx uses URI matching. E.g., if the production frontend is on `/` (root) it needs to be included last, otherwise nginx would route all requests to the frontend.

To workaround the case when there is no config available yet (e.g., before first staging or production is deployed) you can add a `*` to the filename and then it will be skipped if it is not found.

#### Template Documentation
How to create a dynamic URL for the GitLab environment, see [GitLab docs](https://docs.gitlab.com/ee/ci/environments/#example-of-setting-dynamic-environment-urls).
